Block Gmail Spammers

The Google does not distinguish the username@gmail.com and user.name@gmail.com
and us.erna.me@gmail.com, etc. email addresses.
Based on this feature a single Gmail account holder able to create
many user account in Drupal.

This module blocks these attempts and does not allow to register user with the
same Gmail account if the user has already registered with the same Gmail account.

USAGE:
Simply download and enable the module and it works in the background.
You can customize the displayed error message for the user when its
registration attempt is blocked by the module.

The module contains a batch process, which cleans up the user database
and eliminate the existing Gmail spammer accounts.